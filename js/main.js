let oUser = createNewUser();
if (oUser !== null) {
    console.log(`User ${oUser.firstName} ${oUser.lastName} has login ${oUser.getLogin()}`);
    console.log(`User ${oUser.firstName} ${oUser.lastName} has password ${oUser.getPassword()}`);
    console.log(`User ${oUser.firstName} ${oUser.lastName}'s birthday is ${oUser.birthDay.toLocaleDateString()}`);
    console.log(`User ${oUser.firstName} ${oUser.lastName} is ${oUser.getAge()} year(s) old`);
}
else
    console.log(`No user has been created`);




function createNewUser() {
    let sFirstName = inputName("Creating new user. Enter first name");
    if (sFirstName === null) return null;

    let sLastName = inputName("Enter last name");
    if (sLastName === null) return null;

    let dBirthDate = inputDateDMY("Enter birthdate [dd.mm.yyyy]");
    if (dBirthDate === null) return null;

    return {
        _firstName: sFirstName,
        get firstName()      { return this._firstName },
        set firstName(value) { this._firstName = value },

        _lastName: sLastName,
        get lastName()      { return this._lastName },
        set lastName(value) { this._lastName = value },

        _birthDay: dBirthDate,
        get birthDay()      { return this._birthDay },
        set birthDay(value) { this._birthDay = value },

        getLogin: function () { return this._firstName.toLowerCase()[0] + this._lastName.toLowerCase(); },

        getAge: function () {
            let dToday = new Date();
            let nAge = dToday.getFullYear() - this._birthDay.getFullYear();
            if (dToday.getMonth() < this._birthDay.getMonth() || (dToday.getMonth() === this._birthDay.getMonth() && dToday.getDate() < this._birthDay.getDate())) {
                nAge--;
            }
            return nAge;
        },

        getPassword: function () { return this._firstName.toUpperCase()[0] + this._lastName.toLowerCase() + this._birthDay.getFullYear(); }
    };
}




function inputName(sMsg) {
    let sName = "";

    do {
        sName = prompt(sMsg, sName);
    } while (sName !== null && !sName)

    return sName;
}




function inputDateDMY(sMsg, sSep = ".") {
    let sDate = "";
    let dDate;
    let aDate;

    do {
        sDate = prompt(sMsg, sDate);
        if (sDate === null) return null;

        aDate = sDate.split(sSep);
        if (aDate.length !== 3) continue;

        aDate[0] = parseInt(aDate[0], 10);
        aDate[1] = parseInt(aDate[1], 10) - 1;
        aDate[2] = parseInt(aDate[2], 10);

        dDate = new Date(aDate[2], aDate[1], aDate[0]);
    } while (!dDate || Number.isNaN(dDate.getTime()) || dDate.getFullYear() !== aDate[2] || dDate.getMonth() !== aDate[1] || dDate.getDate() !== aDate[0])

    return dDate;
}